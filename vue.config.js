module.exports = {
  publicPath: process.env.NODE_ENV == "production" ? "/dist/" : "/",
  configureWebpack: {
    devtool: "source-map"
  },
  lintOnSave: false,
  devServer: {
    // proxy: "http://localhost:3000",
  }
};
