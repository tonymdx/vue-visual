import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 引入data-v组件库
import dataV from "@jiaminghi/data-view";
Vue.use(dataV);

// 引入elementUI组件库
import "./plugins/element.js";

// 引入request实例
import request from "./utils/request.js";
// 挂载实例到Vue原型上
Vue.prototype.$request = request;

Vue.config.productionTip = false;
export const eventBus = new Vue();

new Vue({
  el: "#app",
  router,
  render: h => h(App)
}).$mount("#app");
