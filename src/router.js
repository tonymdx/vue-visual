import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "layout",
      redirect: {
        name: "index"
      },
      component: () =>
        // 二级路由
        import("./routerview/RouterView.vue"),
      children: [
        {
          path: "/index",
          name: "index",
          component: () => import("./views/index/index.vue")
        },
        {
          path: "/example1",
          name: "example1",
          component: () => import("./views/example1/example1.vue")
        },
        {
          path: "/example2",
          name: "example2",
          component: () => import("./views/example2/example2.vue")
        },
        {
          path: "/example3",
          name: "example3",
          component: () => import("./views/example3/example3.vue")
        },
        {
          path: "/example4",
          name: "example4",
          component: () => import("./views/example4/example4.vue")
        },
        {
          path: "/example5",
          name: "example5",
          component: () => import("./views/example5/example5.vue")
        },
        {
          path: "/example6",
          name: "example6",
          component: () => import("./views/example6/example6.vue")
        },
        {
          path: "/example7",
          name: "example7",
          component: () => import("./views/example7/example7.vue")
        },
        {
          path: "/example8",
          name: "example8",
          component: () => import("./views/example8/example8.vue")
        }
      ]
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("./views/login/login.vue")
    },
    {
      path: "*",
      redirect: {
        name: "index"
      }
    }
  ]
});
