import Vue from "vue";
import Vuex from "vuex";
// vuex 日志插件
import { createLogger } from "vuex";
Vue.use(Vuex);
const debug = process.env.NODE_ENV !== "production";
export default new Vuex.Store({
  // modules: {},
  state: {},
  mutations: {},
  actions: {},
  strict: debug,
  plugins: debug ? [createLogger()] : []
});
