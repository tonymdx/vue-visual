import axios from "axios";
import { Message } from "element-ui";
// create an axios instance
const service = axios.create({
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
});
// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    return config;
  },
  error => {
    // do something with request error

    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    return response.data;
  },
  error => {
    Message({
      message: error.message,
      type: "error",
      duration: 5 * 1000
    });
    // switch (err.response.status) {
    //     // => 错误状态码处理
    //     case 400:
    //         console.log('错误400')
    //         break
    //     case 401:
    //         console.log('错误401')
    //         // 清除本地session
    //         sessionStorage.clear()
    //         // 跳转到登录
    //         window.location.reload();
    //         break
    //     case 403:
    //         Message({
    //             message: '没有相关权限',
    //             type: 'warning'
    //         });
    //     default:
    //         console.log('默认错误')
    // }
    return Promise.reject(error);
  }
);
export default service;
